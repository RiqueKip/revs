<?php
$servername = "localhost";
$username = "root";
$password = " ";
$dbname = "rorok";

// Create connection
$conn = new mysqli("localhost", "root", "" , "rorok");
// Check connectio
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// sql to create table
$sql = "CREATE TABLE votersinfo_Table (
name VARCHAR(30) NOT NULL, 
ID_No INT(8) NOT NULL,
grower_No VARCHAR(30) NOT NULL PRIMARY KEY,
buyingcentre_No VARCHAR(8) NOT NULL,
Phone_No INT(10) NOT NULL,
No_of_Shares INT(8) NOT NULL FOREIGN KEY,
)";

if ($conn->query($sql) === TRUE) {
    echo "Table Voter's information created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>